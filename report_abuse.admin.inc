<?php

/**
 * Admin settings configuration form.
 */
function report_abuse_admin_configuration_settings($form, &$form_state) {
  $form = array();
  $entity_types = array();
  $entity_types_global = entity_get_info();
  $form['report_abuse_entity_types'] = array(
    '#title' => t('Entity types'),
    '#type' => 'fieldset',
    '#description' => t('Select the entity types / bundles on which you want to enable the report abuse flag.'),
  );
  foreach($entity_types_global as $name => $entity_type) {
    $entity_types[$name] = array(
      'label' => $entity_type['label'],
      'bundles' => $entity_type['bundles'],
    );
  }
  foreach ($entity_types as $name => $type) {
    if ($name != 'report_abuse') {
      $options = array();
      $form['report_abuse_entity_types']['text_entity_type_' . $name] = array('#markup' => '<h5>' . $type['label'] . '</h5>');
      if (count($type['bundles']) == 0) {
        $form['report_abuse_entity_types']['report_abuse_entity_type_' . $name] = array(
          '#type' => 'checkbox',
          '#options' => array($name => $type['label']),
          '#default_value' => variable_get('report_abuse_entity_type_' . $name, ''),
        );
      }
      else {
        foreach ($type['bundles'] as $bundle_name => $bundle) {
          $options[$bundle_name] = $bundle['label'];
        }
        $form['report_abuse_entity_types']['report_abuse_entity_type_' . $name] = array(
          '#label' => t($type['label']),
          '#type' => 'checkboxes',
          '#options' => $options,
          '#default_value' => variable_get('report_abuse_entity_type_' . $name, array()),
        );
      }
    }
  }
  return system_settings_form($form);
}

function report_abuse_form($form, &$form_state, $entity, $op, $entity_type) {
  $form = array();
  dsm(func_get_args());
  $form['report_abuse_op'] = array(
    '#markup' => $op . ' ' . $entity_type,
  );
  return $form;
}